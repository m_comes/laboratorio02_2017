package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {

	private static final int MAX_ADDITIONAL_ROLLS = 2;
	private static final int TOTAL_PINS = 10;
	private static final int MAX_ROLLS = 22;
	private int[] rolls;
	private int currentRoll;
	
	public BowlingGame() {
		rolls = new int[MAX_ROLLS];
	}
	
	@Override
	public void roll(int pins) {
		rolls[currentRoll] = pins;
		
		if (isStrike(currentRoll) && currentRoll != 20) {
			rolls[++currentRoll] = 0;
		}
		
		currentRoll++;
	}

	@Override
	public int score() {
		int score = 0;
		
		for	(int i = 0; i < MAX_ROLLS - MAX_ADDITIONAL_ROLLS; i++) {
			score += rolls[i];
			
			if (isStrike(i)) {
				if (isStrike(i + 2) && i != 18) {
					score += rolls[i + 2] + rolls[i + 4];
				} else {
					score += rolls[i + 2] + rolls[i + 3];
				}
				
				i += 1;
			} else if (isSpare(i)) {
				score += rolls[i + 1];
			}
		}
		
		return score;
	}

	private boolean isStrike(int rollNumber) {
		return isFirstRollOfFrame(rollNumber) && rolls[rollNumber] == TOTAL_PINS;
	}

	private boolean isSpare(int rollNumber) {
		return !isFirstRollOfFrame(rollNumber) && rolls[rollNumber - 1] + rolls[rollNumber] == TOTAL_PINS;
	}

	private boolean isFirstRollOfFrame(int rollNumber) {
		return rollNumber % 2 == 0;
	}

}
